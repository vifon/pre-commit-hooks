#!/usr/bin/env -S emacs --batch --load
;;; -*- lexical-binding: t; -*-

(defun check-parens-in-file (file)
  "Run `check-parens' on FILE.

On error returns a cons with FILE in its `car' and the raw error
received from `check-parens' in `cdr'.

Otherwise return nil."
  (with-temp-buffer
    (insert-file-contents file)
    ;; Handle comments etc.
    (emacs-lisp-mode)
    (condition-case err
        (scan-sexps (point-min) (point-max))
      (scan-error
       (goto-char (goto-char (nth 2 err)))
       `(,file
         (,(line-number-at-pos) . ,(current-column))
         ,@err))
      (:success nil))))

(defun check-parens-in-files (files)
  "Return a list of non-nil results of `check-parens-in-file' on FILES."
  (delete nil
          (mapcar #'check-parens-in-file
                  files)))

(defun check-parens-batch-and-exit ()
  "Run `check-parens-in-files' on `command-line-args-left' and exit.

If any errors are reported, exit with a non-zero exit code."
  (let ((errors (check-parens-in-files command-line-args-left)))
    (dolist (err errors)
      (pcase err
        (`(,file (,line . ,col) scan-error ,msg . ,_)
         (message "%s:%d:%d: %s" file line col msg))))
    (kill-emacs (if errors 1 0))))
