#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

# My personal variables controlling my Emacs initialization.
# Not supported by Emacs itself!
export EMACS_NO_LOCAL=1
export EMACS_FORCE_EAGER=1

# Try loading the config in full and exiting immediately to detect any
# obvious startup errors beyond simple syntax errors.

# use-package-expand-minimally is needed to prevent use-package from
# catching errors in its declarations.

emacs --batch \
      --eval "(setq use-package-expand-minimally t)" \
      --load ~/.emacs.d/early-init.el \
      --load ~/.emacs.d/init.el \
      --funcall kill-emacs
