#!/usr/bin/env -S emacs --batch --load
;;; -*- lexical-binding: t; -*-

(let ((straight-path
       (expand-file-name ".emacs.d/straight/repos/"
                         (getenv "HOME"))))
  ;; Check if package-lint is installed with straight.el,
  ;; reuse it if possible.
  (if (file-directory-p straight-path)
      (require 'package-lint
               (expand-file-name "package-lint/package-lint"
                                 straight-path))
    ;; Use package.el otherwise.
    (package-initialize)
    (unless package-archive-contents
      (package-refresh-contents))
    (package-install 'package-lint t)))

(when (string= (car command-line-args-left) "--main-file")
  (pop command-line-args-left)
  (setq package-lint-main-file (file-name-nondirectory (pop command-line-args-left))))

(advice-add 'package-lint--check-packages-installable :override #'always)
(package-lint-batch-and-exit)
