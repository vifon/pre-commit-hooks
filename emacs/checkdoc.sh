#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

CHECKDOC="$(
emacs -Q --batch 2>&1 \
      --eval "(mapc #'checkdoc-file command-line-args-left)" \
      "$@"
)"
if [ -n "$CHECKDOC" ]; then
    printf '%s\n' "$CHECKDOC"
    exit 1
fi
